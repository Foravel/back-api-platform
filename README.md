# Back-end Api Platform

It is an api based on API PLATFORM

## Installation

1. Clone the project

```bash
git clone https://gitlab.com/Foravel/back-api-platform.git
```
2. From the root directory, build the containers
```bash
docker-compose up --build
```
3. Update the database with all entities
```bash
docker-compose exec php bin/console d:s:u --force
```
4. Go to https://localhost:444 

## Troubleshooting


- **php service does not start ?** : Remove the /vendor folder located in the /admin folder and make a composer install.


## License
[MIT](https://choosealicense.com/licenses/mit/)