<?php

// composer require symfony/http-client elasticsearch/elasticsearch

namespace App\Controller;

use App\Entity\Campaign;
use Elasticsearch\ClientBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CampaignController extends AbstractController
{
    private $bookPublishingHandler;
    private $client;
    private $campaign;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function __invoke(Campaign $data): Response
    {

        $this->campaign = $data;
        //Scrap texts and save it in ES DB

        $responseA = $this->client->request(
            'POST',
            'http://api-flask:5000/scrap',
            [
                'json' => [
                    'campaign_id' => $data->getId(),
                    'campaign_keywords' => $data->getKeywords()
                ]
            ]
        );

        /*
        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $contentArr = $response->toArray();
        // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]
        */

        // Retrieve text in ES DB and generate new text from it with markov algorithm

        $hosts = [
            'http://es01:9200'
        ];
        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();
        $params = [
            'index' => 'text',
            'body' => [
                'size' => 1000,
                'query' => [
                    'match' => [
                        'campaign_id' => $data->getId()
                    ]
                ]
            ]
        ];

        $response = $client->search($params);
        $textJoined = '';
        foreach ($response['hits']['hits'] as $hit) {
            $textJoined .= trim($hit['_source']['text']) . ' ';
        }

        $text = $textJoined;
        $text = $this->markovify($text, 1, 2000);
   
        $h1 = $this->buildTitle(1, $data->getTitleParts(), null, $this->campaign);
        $textFinal = $h1['string'];
        $textFinal .= '<img src="'.('https://localhost/media/'.$this->campaign->image[0]->filePath).'"/>';
        $textFinal .= $this->breakLongText($text, 200, 250, $h1['parts'], $this->campaign);

        $entityManager = $this->getDoctrine()->getManager();
        $campaign = $data;
        $campaign->setPreview($textFinal);
        $entityManager->persist($campaign);
        $entityManager->flush();


        return new Response(json_encode($textFinal));
    }

    // Automated text generator using markov chain
    public function markovify($text, $keySize, $maxWords)
    {
        // Create list of tokens
        $token = array();
        $position = 0;
        $maxPosition = strlen($text);
        while ($position < $maxPosition) {
            if (preg_match('/^(\S+)/', substr($text, $position, 25), $matches)) {
                $token[] = $matches[1];
                $position += strlen($matches[1]);
            } elseif (preg_match('/^(\s+)/', substr($text, $position, 25), $matches)) {
                $position += strlen($matches[1]);
            } else {
                die('Unknown token found at position ' . $position . ' : ' .
                    substr($text, $position, 25) . '...' . PHP_EOL);
            }
        }

        // Create Dictionary
        $dictionary = array();
        for ($i = 0; $i < count($token) - $keySize; $i++) {
            $prefix = '';
            $separator = '';
            for ($c = 0; $c < $keySize; $c++) {
                $prefix .= $separator . $token[$i + $c];
                $separator = '.';
            }
            $dictionary[$prefix][] = $token[$i + $keySize];
        }

        // Starting token
        $rand = rand(0, count($token) - $keySize);
        $startToken = array();
        for ($c = 0; $c < $keySize; $c++) {
            array_push($startToken, $token[$rand + $c]);
        }

        // Create Text
        $text = implode(' ', $startToken);
        $words = $keySize;
        do {
            $tokenKey = implode('.', $startToken);
            if (array_key_exists($tokenKey, $dictionary)) {

                $rand = rand(0, count($dictionary[$tokenKey]) - 1);
                $newToken = $dictionary[$tokenKey][$rand];
                $text .= ' ' . $newToken;
                $words++;
                array_shift($startToken);
                array_push($startToken, $newToken);
            } else {
                $words++;
            }
        } while ($words < $maxWords);

        return $text;
    }

    function breakLongText($text, $length = 200, $maxLength = 250, $h1Part = null, $campaign)
    {
        //Text length
        $textLength = strlen($text);

        //initialize empty array to store split text
        $splitText = array();

        //return without breaking if text is already short
        if (!($textLength > $maxLength)) {
            $splitText[] = $text;
            return $splitText;
        }

        //Guess sentence completion
        $needle = '.';

        /*iterate over $text length 
          as substr_replace deleting it*/
        while (strlen($text) > $length) {

            $end = strpos($text, $needle, $length);

            if ($end === false) {

                //Returns FALSE if the needle (in this case ".") was not found.
                $paragraph = substr($text, 0, $end);
                $splitText[] = $this->addTitleToParagraph(2, $campaign->getTitleParts(), $paragraph, $h1Part, $campaign);
                $text = '';
                break;
            }

            $end++;
            $paragraph = substr($text, 0, $end);
            if (rand(0, 3) == 1) {
                $splitText[] = $this->addTitleToParagraph(2, $campaign->getTitleParts(), $paragraph, $h1Part, $campaign);
            } else {
                $splitText[] = $paragraph;
            }
            $text = substr_replace($text, '', 0, $end);
        }

        if ($text) {
            $paragraph = substr($text, 0);
            $splitText[] = '<p>' . $paragraph . '</p>';
        }

        $splitText = implode('', $splitText);

        return $splitText;
    }

    public function buildTitle($level = 1, $titleParts, $paragraph = null, $campaign, $h1Part = null)
    {
        if ($level == 1) {
            $title = [];
            $title['string'] = '';
            foreach ($titleParts as $key => $part) {
                $title['parts'][] = $part[array_rand($part)];
                $title['string'] .= $part[array_rand($part)] . ' ';
            }
            $title['string'] = '<h' . $level . '>' . trim($title['string']) . '</h' . $level . '>';
            return $title;
        } elseif ($level == 2) {
            $title = '';
            // Create an array containing only the keys of True element in subtitleParts[]
            $titlePartsKeys = array_keys(array_filter($campaign->getSubtitleParts(), function ($el) use ($campaign) {
                return ($el);
            }));
            foreach ($titlePartsKeys as $key) {
                $title .= $h1Part[$key] . ' '; // Prefix the h2 with parts of h1 
            }
            $title = trim($title);
            $title .= ' | ';
            foreach ($titlePartsKeys as $key) {
                $title .= $titleParts[$key][array_rand($titleParts[$key])] . ' ';
            }
            $title = trim($title);
            return '<h' . $level . '>' . $title . '</h' . $level . '>';;
        } else {
            $title = '';
            $firstSentenceEnd = strpos($paragraph, '.');
            return '<h' . $level . '>' . substr($paragraph, 0, $firstSentenceEnd) . '</h' . $level . '>';;
        }
    }

    public function addTitleToParagraph($level, $titleParts, $paragraph, $h1Part = null, $campaign)
    {
        return $this->buildTitle($level, $titleParts, $paragraph, $campaign, $h1Part) . '<p>' . $paragraph . '</p>';
    }
}
