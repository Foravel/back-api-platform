<?php

// composer require symfony/http-client

namespace App\Controller;

use stdClass;
use Stripe\StripeClient;
use Symfony\Component\Process\Process;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListProductsAction
{

    private $stripe;

    public function __construct()
    {
        $this->stripe = new \Stripe\StripeClient(
            'sk_test_XLk0yslE9DozPgtmXC4wWk8r'
          );
    }
    
    #[Route(
        name: 'list_stripe_products',
        path: '/stripe-products',
        methods: ['GET']
    )]
    public function list(): JsonResponse
    {
        $products = array();
        $prices = $this->stripe->prices->all(['limit' => 3]);

        foreach($prices->data as $price) {
            $product = $this->stripe->products->retrieve($price->product);
            $product->{'price'} = $price;
            array_push($products, $product);
        }

        return new JsonResponse($products);
    }

    #[Route(
        name: 'update_stripe_product',
        path: '/stripe-update-product',
        methods: ['POST']
    )]
    public function update(Request $request): JsonResponse
    {
        $post = json_decode($request->getContent(), true);
        unset($post['values']['price']);
        
        $res = $this->stripe->products->update(
            $post['productId'],
            $post['values']
        );

        return new JsonResponse($res);
    }

    #[Route(
        name: 'retrieve-stripe-subscription',
        path: '/stripe-retrieve-subscription',
        methods: ['GET']
    )]
    public function getSubscription(Request $request): JsonResponse
    {
        
        $res = $this->stripe->subscriptions->retrieve(
            $request->query->get('id'),
            []
        );

        return new JsonResponse($res);
    }

    #[Route(
        name: 'update-stripe-subscription',
        path: '/stripe-update-subscription',
        methods: ['GET']
    )]
    public function updateSubscription(Request $request): JsonResponse
    {
        
        $res = $this->stripe->subscriptions->update(
            $request->query->get('id'),
            ['cancel_at_period_end' => true]
        );

        return new JsonResponse($res);
    }
}