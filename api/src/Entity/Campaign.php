<?php

namespace App\Entity;

use App\Entity\MediaObject;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\CampaignController;
use App\Repository\CampaignRepository;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CampaignRepository::class)
 */
#[ApiResource(mercure: true, itemOperations: [
    'patch',
    'get',
    'post_publication' => [
        'method' => 'GET',
        'path' => '/campaign/{id}/scrap',
        'controller' => CampaignController::class,
    ],
])]

class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $keywords = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $prefixes = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $suffixes = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $preview;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $titleParts = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $subtitleParts = [];

    /**
     * @var MediaObject|null
     *
     * @ORM\ManyToMany(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     */
    public $image;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeywords(): ?array
    {
        return $this->keywords;
    }

    public function setKeywords(array $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getPrefixes(): ?array
    {
        return $this->prefixes;
    }

    public function setPrefixes(?array $prefixes): self
    {
        $this->prefixes = $prefixes;

        return $this;
    }

    public function getSuffixes(): ?array
    {
        return $this->suffixes;
    }

    public function setSuffixes(?array $suffixes): self
    {
        $this->suffixes = $suffixes;

        return $this;
    }

    public function getPreview(): ?string
    {
        return $this->preview;
    }

    public function setPreview(?string $preview): self
    {
        $this->preview = $preview;
        return $this;
    }

    public function getTitleParts(): ?array
    {
        return $this->titleParts;
    }

    public function setTitleParts(?array $titleParts): self
    {
        $this->titleParts = $titleParts;

        return $this;
    }

    public function getSubtitleParts(): ?array
    {
        return $this->subtitleParts;
    }

    public function setSubtitleParts(?array $subtitleParts): self
    {
        $this->subtitleParts = $subtitleParts;

        return $this;
    }

}
