<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\DiscussionRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=DiscussionRepository::class)
 */
#[ApiResource(
    itemOperations: [
        'patch',
        'get',
    ],
    mercure: true,
    normalizationContext: ['groups' => ['discussion']],
)
]
class Discussion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"discussion"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="discussion")
     * @Groups({"discussion", "user"})
     * 
     */
    private $messages;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"discussion", "user"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="discussions")
     * @Groups({"discussion", "user", "message"})
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"discussion", "user"})
     */
    private $query;
    
    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"user", "discussion"})
     */
    private $lastlyOpenedBy = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user"})
     */
    private $isResolved;
    

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setDiscussion($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getDiscussion() === $this) {
                $message->setDiscussion(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(?string $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function getLastlyOpenedBy(): ?array
    {
        return $this->lastlyOpenedBy;
    }

    public function setLastlyOpenedBy(?array $lastlyOpenedBy): self
    {
        $this->lastlyOpenedBy = $lastlyOpenedBy;

        return $this;
    }

    public function getIsResolved(): ?bool
    {
        return $this->isResolved;
    }

    public function setIsResolved(?bool $isResolved): self
    {
        $this->isResolved = $isResolved;

        return $this;
    }




}
