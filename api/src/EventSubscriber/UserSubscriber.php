<?php 

namespace App\EventSubscriber;

use Stripe\Stripe;
use App\Entity\User;
use Stripe\StripeClient;
use App\Entity\Discussion;
use App\Repository\UserRepository;
use Symfony\Component\Mercure\Update;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class UserSubscriber implements EventSubscriberInterface
{

    private $entityManager;
    private $userRepo;
    private $bus;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepo, MessageBusInterface $bus)
    {
        $this->entityManager = $entityManager;
        $this->userRepo = $userRepo;
        $this->bus = $bus;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['createStripeCustomer', EventPriorities::POST_WRITE],
        ];
    }

    public function createStripeCustomer(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$user instanceof User || Request::METHOD_POST !== $method) {
            return;
        }

        if($user->getSource() === 'chatbox') {
            return;
        }
        
        $stripe = new \Stripe\StripeClient(
            'sk_test_XLk0yslE9DozPgtmXC4wWk8r'
        );

        $customer = $stripe->customers->create([
            'email' => $user->getEmail(),
            'name' => $user->getFirstName() . ' ' . $user->getLastName(),
            'phone' => $user->getPhone()
        ]);
        
        $subscription = $stripe->subscriptions->create([
            'customer' => $customer->id,
            'items' => [
              ['price' => $user->getStripePriceId()], 
            ],
            'payment_behavior' => 'default_incomplete',
            'expand' => ['latest_invoice.payment_intent'],
          ]);
        

        $user->setClientSecret($subscription->latest_invoice->payment_intent->client_secret);
        $user->setStripeSubscriptionId($subscription->id);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}