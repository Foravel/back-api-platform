<?php

namespace App\Repository;

use App\Entity\LoggedUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LoggedUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoggedUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoggedUser[]    findAll()
 * @method LoggedUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoggedUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LoggedUser::class);
    }

    // /**
    //  * @return LoggedUser[] Returns an array of LoggedUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LoggedUser
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
